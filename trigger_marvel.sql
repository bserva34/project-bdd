SET SERVEROUTPUT ON;

CREATE OR REPLACE TRIGGER mort 
	AFTER UPDATE OF MORT ON PERSONNAGE for each row 
DECLARE	
	MESSAGE EXCEPTION;
	name varchar(15):= :old.SUPER_NOM;
	big int;
	CURSOR groupe IS SELECT NOM_GR FROM APPARTIENT WHERE SUPER_N=name;
	stock varchar(30);
BEGIN 
	OPEN groupe;
	LOOP 
		FETCH groupe INTO stock;
		EXIT WHEN groupe%NOTFOUND;
		big:=KING(name,stock);
		IF big=1 THEN
			NEW_BOSS_RANDOM(stock);
			dbms_output.Put_line('un nouveau boss a ete declarer pour le groupe '|| stock);
		END IF;		
	END LOOP;
	CLOSE groupe;	
END;
/


CREATE OR REPLACE TRIGGER UPGRADE_POUVOIR 
	BEFORE UPDATE ON POUVOIR for each row
DECLARE
	MESSAGE EXCEPTION;
	MSG varchar(30):='impossible veuiller appeller';	
BEGIN
	RAISE MESSAGE;
	EXCEPTION
	WHEN MESSAGE THEN
	RAISE_APPLICATION_ERROR(-20324,MSG);	
END;
/

