SET SERVEROUTPUT ON;

/* Pour appeler la procedure métier*/
prompt"affichage du métier de IRON-MAN"
DECLARE 
	NOM VARCHAR(15):='IRON-MAN';
	LEMETIER VARCHAR(30);
	a varchar(10):='metier :';
BEGIN 
	
	metier(NOM,LEMETIER);
	dbms_output.Put_line(a);
	dbms_output.Put_line(LEMETIER);
END;
/

/*pour update la base de donnée et retirer la fonction de chef appel ENLEVER_BOSS*/
SELECT * FROM APPARTIENT WHERE NOM_GR='AVENGERS' AND SUPER_N='HULK';
prompt"enlever le statut de boss a HULK dans le groupe AVENGERS "
DECLARE
	NAME VARCHAR(15):='HULK';
	GROUPE VARCHAR(30):='AVENGERS';
BEGIN
	ENLEVER_BOSS(NAME,GROUPE);
END;
/
SELECT * FROM APPARTIENT WHERE NOM_GR='AVENGERS' AND SUPER_N='HULK';


/*pour update la base de donnée et donner un nouveau chef*/
SELECT * FROM APPARTIENT WHERE NOM_GR='AVENGERS' AND SUPER_N='IRON-MAN';
prompt"déclaration du statut boss pour IRON-MAN dans les AVENGERS"
DECLARE
	NAME VARCHAR(15):='IRON-MAN';
	GROUPE VARCHAR(30):='AVENGERS';
BEGIN
	NEW_BOSS(NAME,GROUPE);
END;
/
SELECT * FROM APPARTIENT WHERE NOM_GR='AVENGERS' AND SUPER_N='IRON-MAN';


/*appel pour moyenne caracteristique*/
SELECT * FROM CARACTERISTIQUE WHERE PERSONNAGE='IRON-MAN';
prompt"moyenne des caractéristique de IRON-MAN"
DECLARE 
	NAME VARCHAR(15):='IRON-MAN';
	res real;
BEGIN 
	res:=MOY_CAP(NAME);
	dbms_output.Put_line(res);
END;
/


/*appele id*/
prompt"donne l'identite de IRON-MAN"
DECLARE
	name VARCHAR(15):='IRON-MAN';
	n VARCHAR(15);
	p VARCHAR(15);
BEGIN
	ID(name,n,p);
	dbms_output.Put_line('veritable identite de ' || name || ' est ' || n || ' ' || p);	
END;
/


/*appel l affichage d'un groupe*/
prompt"affiche tous les membres du groupe AVENGERS"
DECLARE 
	GR VARCHAR(30):='AVENGERS';
BEGIN 
	AFFICHAGE_GROUPE(GR);
END;
/


/*requete pour déclancher le trigger mort*/
prompt"déclanchement trigger mort / update pour annoncer la mort de IRON-MAN"
UPDATE PERSONNAGE SET MORT='17-10-2023' WHERE SUPER_NOM='IRON-MAN';
SELECT * FROM APPARTIENT WHERE NOM_GR='AVENGERS' OR NOM_GR='CIVILWAR GROUPE 2' OR NOM_GR='STARK INDUSTRIES';

/*requete pour déclancher le trigger upgrade_pouvoir*/
prompt"déclanchement trigger upgrade_pouvoir"
UPDATE POUVOIR SET DESCRIPTION='resistance incroyable et manie la foudre' WHERE NOM_POUVOIR='DIEU';

/*utilisation des deux procédure pour améliorer un pouvoir dans ce cas on veut le modifier pour un seul héros*/
prompt"utilisation des procédure pour créer le pouvoir et le lier au personnage concerné ici c'est THOR"
DECLARE
	NOM VARCHAR(15):='DIEUBIS';
	DESCRIPTION VARCHAR(100):='Resistance incroyable et manie la foudre';
	T VARCHAR(15):='MAGIQUE';
BEGIN
	NEW_POUVOIR(NOM,DESCRIPTION,T);
END;
/


DECLARE 
	NOM VARCHAR(15):='DIEUBIS';
	N VARCHAR(15):='THOR';
	ORIGINE VARCHAR(100):='Fils de Odin, dieu du tonnere';
BEGIN
	LIAISON_POUVOIR(N,NOM,ORIGINE);
END;
/


